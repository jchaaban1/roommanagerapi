FROM maven:3.8.3-openjdk-17 AS build
WORKDIR /app
COPY pom.xml .
COPY src src/
RUN mvn package

FROM openjdk:17-jdk-slim
WORKDIR /app
COPY --from=build /app/target/code-challenge.jar .
EXPOSE 8080
CMD ["java", "-jar", "code-challenge.jar"]
