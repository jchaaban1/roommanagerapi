# Backend Code Challenge

## Introduction
This project is a Java/Spring Boot-based application focused on building a REST API with file import functionality and
room searching capabilities.
The primary goal is to manage multiple rooms in a building, each containing a number of persons.
Each person can only belong to one room. The rooms have specific formats for their numbers and names.
Each person has a name consisting of a firstname, lastname, an optional name addition, and a title. Additionally,
each person has an LDAP identifier consisting of the first letter of their firstname and their lastname.
The application validates these constraints and processes the information from a CSV file uploaded through a REST API.

The main focus of this project is on comprehensive unit, integration, and mutation tests to ensure the robustness and
reliability of the application.


## Table of Contents
- [Introduction](#introduction)
- [Installation](#installation)
- [Usage](#usage)
- [Features](#features)
- [Dependencies](#dependencies)
- [Running unit test](#Running unit test)
- [Running mutation test](#Running mutation test)


## Installation
To install and run the project, follow these steps:

1. Clone the repository:
    ```bash
    git clone url
    cd dir
    ```

2. Build the project using Maven:
    ```bash
    mvn clean install
    ```

3. Run the application:
    ```bash
    mvn spring-boot:run
    ```

## Usage
To use the application, follow these steps:

1. Upload a CSV file containing the room and person data via the provided REST API endpoint (see: src/test/resources/csv/sitzplan.csv).
2. The application will validate the content of the CSV file.

## Features
- **Room Management**: Manage rooms with specific formats for their numbers and names.
- **Person Management**: Ensure each person is unique to a room and has valid data (title, firstname, lastname, ldap).
- **CSV File Upload**: Upload and validate CSV files containing room and person data.
- **Search Functionality**: Search for rooms.
- **Testing**: Comprehensive unit, mutation, and integration tests.
- **CI/CD**: Minimal GitLab CI pipeline setup for build and test stages.
- **Sweager UI Setup**: Sweager UI Setup.
- **Dockerization**: Dockerized application with an OpenAPI specification.

## Dependencies
- Java 17
- Spring Boot 3.3.0
- Maven
- Docker
- GitLab CI

## Swagger UI
Link to Swagger UI: [Swagger UI](http://localhost:8080/swagger-ui/index.html)


## Running unit and integration tests
To run unit and integration tests, use the following command:
```bash
mvn clean test-compile surefire:test failsafe:integration-test failsafe:verify
```

## Running mutation test
To run the unit tests, use the following command:
```bash
mvn org.pitest:pitest-maven:mutationCoverage
```