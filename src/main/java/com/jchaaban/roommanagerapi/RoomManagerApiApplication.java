package com.jchaaban.roommanagerapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RoomManagerApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(RoomManagerApiApplication.class, args);
    }

}
