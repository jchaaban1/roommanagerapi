package com.jchaaban.roommanagerapi.entity;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;

@Getter
public class Person {

    @JsonProperty("first name")
    @Schema(
            description = "The first name of the person (note a person could have multiple firstnames)",
            example = "Jaafar"
    )
    private String firstName;

    @JsonProperty("last name")
    @Schema(
            description = "The last name of the person.",
            example = "Chaaban"
    )
    private String lastname;

    @JsonProperty("ldap")
    @Schema(
            description = "The LDAP identifier of the person, that consist of the first letter of its firstname and the lastname",
            example = "jchaaban"
    )
    private String ldap;

    @JsonProperty("title")
    @Schema(
            description = "The title of the person, indicates if the person has a Dr. title",
            example = "Dr."
    )
    private String title;

    @JsonProperty("name addition")
    @Schema(
            description = "Any Name addition the person has. Could be one of the following values (de, van, von)",
            example = "von"
    )
    private String nameAddition;

    public Person(
            String title,
            String firstName,
            String nameAddition,
            String lastname,
            String ldap
    ) {
        this.firstName = firstName;
        this.lastname = lastname;
        this.ldap = ldap;
        this.title = title;
        this.nameAddition = nameAddition;
    }

    @Override
    public String toString() {
        return System.lineSeparator() + "firstname: " + firstName
        + " lastname: " + lastname
        + " ldap: " + ldap
        + " title: " + title
        + " nameAddition: " + nameAddition;
    }
}
