package com.jchaaban.roommanagerapi.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
@JsonPropertyOrder({"room", "people"})
@Schema(description = "Represents a room entity containing the room number and a list of people.")
public class Room {

    @JsonProperty("room")
    @Schema(description = "The unique number of the room.", example = "1111")
    private final String roomNumber;

    @JsonProperty("people")
    @Schema(description = "List of people assigned to this room.")
    private final List<Person> people;

    @JsonCreator
    public Room(@JsonProperty("room") String roomNumber) {
        this.roomNumber = roomNumber;
        this.people = new ArrayList<>();
    }

    @Schema(description = "Method to add a person to the room.")
    public void addPerson(Person person) {
        people.add(person);
    }

    @Override
    public String toString() {
        return "Room{"
                + "roomNumber='"
                + roomNumber + '\''
                + ", people="
                + people
                + '}';
    }
}
