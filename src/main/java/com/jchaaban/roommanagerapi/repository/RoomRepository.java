package com.jchaaban.roommanagerapi.repository;

import com.jchaaban.roommanagerapi.entity.Room;
import com.jchaaban.roommanagerapi.rest.exception.room.RoomNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Component
public class RoomRepository {
    private Map<String, Room> rooms;

    public RoomRepository() {
        this.rooms = new LinkedHashMap<>();
    }

    public void add(Room room) {
        String roomNumber = room.getRoomNumber();
        rooms.put(roomNumber, room);
    }

    public Room fetchRoom(String roomNumber) throws RoomNotFoundException {
        Room room = rooms.get(roomNumber);

        if (null != room) {
            return room;
        }

        throw new RoomNotFoundException(roomNumber);
    }

    public List<Room> fetchAllRooms() {
        return new ArrayList<>(rooms.values());
    }

    public void reset() {
        rooms = new LinkedHashMap<>();
    }
}
