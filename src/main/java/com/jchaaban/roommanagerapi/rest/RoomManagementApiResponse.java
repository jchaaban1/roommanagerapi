package com.jchaaban.roommanagerapi.rest;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;

@Getter
@Schema(description = "Represents the API response in case of an error, including error codes and messages.")
public class RoomManagementApiResponse {
    @Schema(description = "The error code associated with the response.", example = "5")
    private int code;

    @Schema(description = "The message describing the response or error.", example = "Room not found")
    private final String message;

    public RoomManagementApiResponse(String message, int errorCode) {
        this.code = errorCode;
        this.message = message;
    }

    public RoomManagementApiResponse(String message) {
        this.message = message;
    }

}
