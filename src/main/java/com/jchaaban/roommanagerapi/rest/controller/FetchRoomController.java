package com.jchaaban.roommanagerapi.rest.controller;

import com.jchaaban.roommanagerapi.entity.Room;
import com.jchaaban.roommanagerapi.rest.RoomManagementApiResponse;
import com.jchaaban.roommanagerapi.rest.exception.room.InvalidRoomNumberException;
import com.jchaaban.roommanagerapi.rest.exception.room.RoomNotFoundException;
import com.jchaaban.roommanagerapi.service.FetchRoomService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/room")
public class FetchRoomController {

    public static final int INVALID_ROOM_NUMBER_FORMAT = 6;
    public static final int ROOM_NOT_FOUND = 5;
    private final FetchRoomService fetchRoomUseCae;

    public FetchRoomController(FetchRoomService fetchRoomUseCae) {
        this.fetchRoomUseCae = fetchRoomUseCae;
    }

    @Operation(summary = "fetch all rooms", description = "fetch all rooms")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful retrieval of rooms",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Room.class))
            )
    })
    @GetMapping
    public ResponseEntity<List<Room>> fetchAllRooms() {
        List<Room> rooms = fetchRoomUseCae.fetchAllRooms();
        return new ResponseEntity<>(rooms, HttpStatus.OK);
    }

    @Operation(summary = "fetch a room", description = "fetch a specific room using its number")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200", description = "Successful retrieval of the room",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Room.class))
            ),
            @ApiResponse(
                    responseCode = "404", description = "Room not found",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = RoomManagementApiResponse.class),
                            examples = @ExampleObject(value = "{ \"code\": 5, \"message\": \"Room not found\" }"))
            ),
            @ApiResponse(
                    responseCode = "400", description = "Invalid room number format",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = RoomManagementApiResponse.class),
                            examples = @ExampleObject(value = "{ \"code\": 6, \"message\": \"Invalid room number format\" }"))
            )
    })
    @GetMapping("/{number}")
    public ResponseEntity<Room> fetchRoom(
            @Parameter(description = "The number of the room to be fetched", required = true)
            @PathVariable String number
    ) throws RoomNotFoundException, InvalidRoomNumberException {
        Room room = fetchRoomUseCae.fetchRoom(number);
        return new ResponseEntity<>(room, HttpStatus.OK);
    }

    @ExceptionHandler(InvalidRoomNumberException.class)
    public ResponseEntity<RoomManagementApiResponse> handleInvalidRoomNumberException(
            InvalidRoomNumberException invalidRoomNumberException
    ) {
        RoomManagementApiResponse apiResponse = new RoomManagementApiResponse(invalidRoomNumberException.getMessage(), INVALID_ROOM_NUMBER_FORMAT);
        return new ResponseEntity<>(apiResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(RoomNotFoundException.class)
    public ResponseEntity<RoomManagementApiResponse> handleRoomNotFoundException(
            RoomNotFoundException roomNotFoundException
    ) {
        RoomManagementApiResponse apiResponse = new RoomManagementApiResponse(roomNotFoundException.getMessage(), ROOM_NOT_FOUND);
        return new ResponseEntity<>(apiResponse, HttpStatus.NOT_FOUND);
    }
}
