package com.jchaaban.roommanagerapi.rest.controller;

import com.jchaaban.roommanagerapi.rest.RoomManagementApiResponse;
import com.jchaaban.roommanagerapi.rest.exception.file.FileArgumentNotFoundException;
import com.jchaaban.roommanagerapi.rest.exception.person.*;
import com.jchaaban.roommanagerapi.rest.exception.room.DuplicatedRoomException;
import com.jchaaban.roommanagerapi.rest.exception.room.InvalidRoomNumberException;
import com.jchaaban.roommanagerapi.service.FileImportService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/api")
public class UploadFileController {
    public static final int DUPLICATE_PERSON_ERROR_CODE = 3;
    public static final int DUPLICATE_ROOM_ERROR_CODE = 2;
    public static final int INVALID_PERSON_ERROR_CODE = 4;
    public static final int INVALID_ROOM_NUMBER_FORMAT = 6;

    private final FileImportService importUseCase;

    public UploadFileController(FileImportService importUseCase) {
        this.importUseCase = importUseCase;
    }

    @Operation(summary = "Import file", description = "Upload a file for processing")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The file was uploaded successfully",
                    content = @Content(mediaType = "text/plain")),
            @ApiResponse(responseCode = "400", description = "Duplicate room found",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = RoomManagementApiResponse.class),
                            examples = @ExampleObject(value = "{ \"code\": 2, \"message\": \"Duplicate room found\" }"))),
    })
    @PostMapping("/import")
    public ResponseEntity<String> importFile(
            @Parameter(description = "The file to be uploaded", required = true)
            @RequestPart(value = "file", required = false) MultipartFile multipartFile) throws
            IOException,
            DuplicatedRoomException,
            DuplicatedPersonException, FileArgumentNotFoundException {

        if (null == multipartFile) {
            throw new FileArgumentNotFoundException();
        }

        String fileContent = new String(multipartFile.getBytes());
        importUseCase.execute(fileContent);
        return new ResponseEntity<>("The file was uploaded successfully", HttpStatus.OK);
    }

    @ExceptionHandler(DuplicatedRoomException.class)
    public ResponseEntity<RoomManagementApiResponse> handleDuplicateRoomException(DuplicatedRoomException duplicatedRoomException) {
        RoomManagementApiResponse apiResponse = new RoomManagementApiResponse(duplicatedRoomException.getMessage(), DUPLICATE_ROOM_ERROR_CODE);
        return new ResponseEntity<>(apiResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DuplicatedPersonException.class)
    public ResponseEntity<RoomManagementApiResponse> handleDuplicatePersonException(DuplicatedPersonException duplicatedPersonException) {
        RoomManagementApiResponse apiResponse = new RoomManagementApiResponse(duplicatedPersonException.getMessage(), DUPLICATE_PERSON_ERROR_CODE);
        return new ResponseEntity<>(apiResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({
            MissingPersonDataException.class,
            InvalidLdapException.class,
            InvalidLastnameException.class,
            InvalidFirstnameException.class
    })
    public ResponseEntity<RoomManagementApiResponse> handleInvalidPersonDataException(InvalidPersonDataException validationException) {
        RoomManagementApiResponse apiResponse = new RoomManagementApiResponse(validationException.getMessage(), INVALID_PERSON_ERROR_CODE);
        return new ResponseEntity<>(apiResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(FileArgumentNotFoundException.class)
    public ResponseEntity<RoomManagementApiResponse> handleMissingFilePartInRequestException(FileArgumentNotFoundException fileArgumentNotFound) {
        RoomManagementApiResponse apiResponse = new RoomManagementApiResponse(fileArgumentNotFound.getMessage(), 0);
        return new ResponseEntity<>(apiResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvalidRoomNumberException.class)
    public ResponseEntity<RoomManagementApiResponse> handleInvalidRoomNumberException(InvalidRoomNumberException invalidRoomNumberException) {
        RoomManagementApiResponse apiResponse = new RoomManagementApiResponse(invalidRoomNumberException.getMessage(), INVALID_ROOM_NUMBER_FORMAT);
        return new ResponseEntity<>(apiResponse, HttpStatus.BAD_REQUEST);
    }
}
