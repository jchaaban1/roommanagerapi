package com.jchaaban.roommanagerapi.rest.exception.file;

public class FileArgumentNotFoundException extends Exception {
    public FileArgumentNotFoundException() {
        super("No file was uploaded");
    }
}
