package com.jchaaban.roommanagerapi.rest.exception.person;

import java.util.List;

public class DuplicatedPersonException extends InvalidPersonDataException {
    public DuplicatedPersonException(List<String> notUniqueLdaps) {
        super("The following persons are not unique: " + String.join(",", notUniqueLdaps));
    }
}
