package com.jchaaban.roommanagerapi.rest.exception.person;


public class InvalidFirstnameException extends InvalidPersonDataException {
    public InvalidFirstnameException(String[] personDataList) {
        super("Invalid firstname in: " + String.join(" ", personDataList));
    }
}
