package com.jchaaban.roommanagerapi.rest.exception.person;

public class InvalidLastnameException extends InvalidPersonDataException {
    public InvalidLastnameException(String[] personDataList) {
        super("Invalid lastname in: " + String.join(" ", personDataList));
    }
}
