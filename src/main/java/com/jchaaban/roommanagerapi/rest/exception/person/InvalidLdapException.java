package com.jchaaban.roommanagerapi.rest.exception.person;

public class InvalidLdapException extends InvalidPersonDataException {

    public InvalidLdapException(String[] personDataList) {
        super("Invalid Ldap in: " + String.join(" ", personDataList));
    }
}
