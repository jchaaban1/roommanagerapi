package com.jchaaban.roommanagerapi.rest.exception.person;

public class InvalidPersonDataException extends RuntimeException {

    public InvalidPersonDataException(String message) {
        super(message);
    }
}
