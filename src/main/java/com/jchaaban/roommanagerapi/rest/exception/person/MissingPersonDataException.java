package com.jchaaban.roommanagerapi.rest.exception.person;

public class MissingPersonDataException extends InvalidPersonDataException {
    public MissingPersonDataException(String[] personDataList) {
            super("Missing Person data in: " + String.join(" ", personDataList));
    }
}
