package com.jchaaban.roommanagerapi.rest.exception.room;

import com.jchaaban.roommanagerapi.rest.exception.person.InvalidPersonDataException;

import java.util.List;

public class DuplicatedRoomException extends InvalidPersonDataException {
    public DuplicatedRoomException(List<String> duplicatedRoomNumbers) {
        super("The following room numbers are duplicated: " + String.join(",", duplicatedRoomNumbers));
    }
}
