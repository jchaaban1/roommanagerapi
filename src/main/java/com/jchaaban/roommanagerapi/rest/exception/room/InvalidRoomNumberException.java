package com.jchaaban.roommanagerapi.rest.exception.room;

import com.jchaaban.roommanagerapi.rest.exception.person.InvalidPersonDataException;

public class InvalidRoomNumberException extends InvalidPersonDataException {
    public InvalidRoomNumberException(String roomNumber) {
        super(
                "The room number: "
                        + roomNumber
                        + " is invalid, the room number must be exactly four characters long, "
                        + "don't contain any spaces and should contain numbers"
        );
    }
}
