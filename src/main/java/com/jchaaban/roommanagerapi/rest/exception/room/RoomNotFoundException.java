package com.jchaaban.roommanagerapi.rest.exception.room;
public class RoomNotFoundException extends Exception {
    public RoomNotFoundException(String roomNumber) {
        super("The room having number: " + roomNumber + " does not exist");
    }
}
