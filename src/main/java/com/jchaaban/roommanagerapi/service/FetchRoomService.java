package com.jchaaban.roommanagerapi.service;

import com.jchaaban.roommanagerapi.entity.Room;
import com.jchaaban.roommanagerapi.repository.RoomRepository;
import com.jchaaban.roommanagerapi.rest.exception.room.RoomNotFoundException;
import com.jchaaban.roommanagerapi.service.validator.RoomNumberValidator;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FetchRoomService {
    private final RoomRepository roomRepository;
    private final RoomNumberValidator roomNumberValidator;

    public FetchRoomService(RoomRepository roomRepository, RoomNumberValidator roomNumberValidator) {
        this.roomRepository = roomRepository;
        this.roomNumberValidator = roomNumberValidator;
    }

    public Room fetchRoom(String roomNumber) throws RoomNotFoundException {
        roomNumberValidator.validate(roomNumber);
        return roomRepository.fetchRoom(roomNumber);
    }

    public List<Room> fetchAllRooms() {
        return roomRepository.fetchAllRooms();
    }
}
