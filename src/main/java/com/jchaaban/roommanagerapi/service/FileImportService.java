package com.jchaaban.roommanagerapi.service;

import com.jchaaban.roommanagerapi.repository.RoomRepository;
import com.jchaaban.roommanagerapi.rest.exception.person.DuplicatedPersonException;
import com.jchaaban.roommanagerapi.rest.exception.room.DuplicatedRoomException;
import com.jchaaban.roommanagerapi.service.extractor.RoomExtractorService;
import com.jchaaban.roommanagerapi.service.validator.Validator;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FileImportService {
    private final RoomRepository roomRepository;
    private final RoomExtractorService roomExtractor;
    private final List<Validator> validators;


    public FileImportService(RoomRepository roomRepository, RoomExtractorService roomExtractor, List<Validator> validators) {
        this.roomRepository = roomRepository;
        this.roomExtractor = roomExtractor;
        this.validators = validators;
    }

    public void execute(String fileContent) throws DuplicatedRoomException, DuplicatedPersonException {
        List<String> lines = fileContent.lines().toList();
        validators.forEach(validator -> validator.validate(lines));
        roomRepository.reset();
        lines.stream()
                .map(roomExtractor::toRoom)
                .forEach(roomRepository::add);
    }

}
