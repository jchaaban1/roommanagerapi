package com.jchaaban.roommanagerapi.service.extractor;

import com.jchaaban.roommanagerapi.entity.Person;
import com.jchaaban.roommanagerapi.service.validator.PersonValidator;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class PersonExtractorService {

    public static final String SPACE = " ";
    public static final String DR_TITLE = "Dr.";
    private static final List<String> NAME_ADDITIONS = List.of("von", "van", "de");
    private final List<PersonValidator> personValidators;

    public PersonExtractorService(List<PersonValidator> personValidators) {
        this.personValidators = personValidators;
    }

   public Person toPerson(String lineElements) {
        String[] personDataList = lineElements.trim().split(SPACE);

        personValidators.forEach(personValidator -> personValidator.validate(personDataList));

        String title = extractTitle(personDataList);
        String firstname = extractFirstName(personDataList);
        String lastname = extractLastName(personDataList);
        String ldap = extractLdap(personDataList);
        String nameAddition = extractNameAddition(personDataList);

        return new Person(title, firstname, nameAddition, lastname, ldap);
    }

    private String extractNameAddition(String[] personDataList) {
        if (hasNameAddition(personDataList)) {
            return personDataList[personDataList.length - 3];
        }
        return "";
    }

    private boolean hasNameAddition(String[] personDataList) {
        return NAME_ADDITIONS.contains(personDataList[personDataList.length - 3]);
    }

    private String extractTitle(String[] personDataList) {
        if (hasTitle(personDataList)) {
            return DR_TITLE;
        }
        return "";
    }

    private String extractLastName(String[] personDataList) {
        int lastNameIndex = personDataList.length - 2;
        return personDataList[lastNameIndex];
    }

    private String extractFirstName(String[] personDataList) {

        int firstStartIndex = determineFirstStartIndex(personDataList);

        int firstnameEndIndex = determineFirstnameEndIndex(personDataList);

        String[] firstnames = Arrays.copyOfRange(personDataList, firstStartIndex, firstnameEndIndex + 1);

        return String.join(SPACE, firstnames);
    }

    private int determineFirstnameEndIndex(String[] personDataList) {
        int firstnameEndIndex = personDataList.length - 3;
        if (hasNameAddition(personDataList)) {
            --firstnameEndIndex;
        }
        return firstnameEndIndex;
    }

    private int determineFirstStartIndex(String[] personDataList) {
        int firstStartIndex = 0;

        if (hasTitle(personDataList)) {
            ++firstStartIndex;
        }
        return firstStartIndex;
    }

    private boolean hasTitle(String[] personDataList) {
        return personDataList[0].equals(DR_TITLE);
    }

    private String extractLdap(String[] personDataList) {
        int ladpIndex = personDataList.length - 1;
        return personDataList[ladpIndex].replace("(", "").replace(")", "");
    }
}
