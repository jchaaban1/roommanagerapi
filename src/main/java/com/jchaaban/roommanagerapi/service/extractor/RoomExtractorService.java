package com.jchaaban.roommanagerapi.service.extractor;

import com.jchaaban.roommanagerapi.entity.Room;
import com.jchaaban.roommanagerapi.service.validator.RoomNumberValidator;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class RoomExtractorService {

    public static final String COMMA = ",";
    public static final int ROOM_NUMBER_INDEX = 0;
    private final PersonExtractorService personExtractor;
    private final RoomNumberValidator roomNumberValidator;

    public RoomExtractorService(PersonExtractorService personExtractor, RoomNumberValidator roomNumberValidator) {
        this.personExtractor = personExtractor;
        this.roomNumberValidator = roomNumberValidator;
    }

    public Room toRoom(String line) {
        String[] lineElements = line.split(COMMA);
        String roomNumber = lineElements[ROOM_NUMBER_INDEX];
        roomNumberValidator.validate(roomNumber);
        Room room = new Room(roomNumber);
        Arrays.stream(lineElements)
                .skip(1)
                .map(personExtractor::toPerson)
                .forEach(room::addPerson);
        return room;
    }


}
