package com.jchaaban.roommanagerapi.service.validator;

import com.jchaaban.roommanagerapi.rest.exception.person.InvalidFirstnameException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Component
public class FirstnameValidator extends PersonValidator {
    private static final int PERSON_TITLE_INDEX = 0;
    private static final int MIN_PERSON_DATA_ELEMENTS = 3;
    private static final String ALLOWED_TITLE = "Dr.";
    private static final List<String> ALLOWED_NAME_ADDITIONS = new ArrayList<>(Arrays.asList("von", "van", "de"));

    public void processValidation(String[] personDataList) throws InvalidFirstnameException {
        int personDataListLength = personDataList.length;

        if (isValidPersonDataLength(personDataListLength)) {
            boolean titleExists = isTitleValid(personDataList);
            boolean nameAdditionExists = isNameAdditionValid(personDataList, personDataListLength);

            if (
                    shouldThrowForMinElements(personDataListLength, titleExists, nameAdditionExists)
                    || shouldThrowForMinElementsPlusOne(personDataListLength, titleExists, nameAdditionExists)
            ) {
                throw new InvalidFirstnameException(personDataList);
            }
        }
    }

    private boolean isValidPersonDataLength(int length) {
        return length == MIN_PERSON_DATA_ELEMENTS || length == MIN_PERSON_DATA_ELEMENTS + 1;
    }

    private boolean isTitleValid(String[] personDataList) {
        return ALLOWED_TITLE.equals(personDataList[PERSON_TITLE_INDEX]);
    }

    private boolean isNameAdditionValid(String[] personDataList, int personDataListLength) {
        return ALLOWED_NAME_ADDITIONS.contains(personDataList[personDataListLength - MIN_PERSON_DATA_ELEMENTS]);
    }

    private boolean shouldThrowForMinElements(int personDataListLength, boolean titleExists, boolean nameAdditionExists) {
        return personDataListLength == MIN_PERSON_DATA_ELEMENTS && (titleExists || nameAdditionExists);
    }

    private boolean shouldThrowForMinElementsPlusOne(int personDataListLength, boolean titleExists, boolean nameAdditionExists) {
        return personDataListLength == MIN_PERSON_DATA_ELEMENTS + 1 && titleExists && nameAdditionExists;
    }
}
