package com.jchaaban.roommanagerapi.service.validator;

import com.jchaaban.roommanagerapi.rest.exception.person.InvalidLastnameException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class LastnameValidator extends PersonValidator {

    private static final List<String> ALLOWED_NAME_ADDITIONS = new ArrayList<>(Arrays.asList("von", "van", "de"));

    @Override
    public void processValidation(String[] personDataList) {
        if (ALLOWED_NAME_ADDITIONS.contains(personDataList[personDataList.length - 2])) {
            throw new InvalidLastnameException(personDataList);
        }
    }
}
