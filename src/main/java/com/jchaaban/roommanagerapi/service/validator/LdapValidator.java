package com.jchaaban.roommanagerapi.service.validator;

import com.jchaaban.roommanagerapi.rest.exception.person.InvalidLdapException;
import org.springframework.stereotype.Component;
@Component
public class LdapValidator extends PersonValidator {
    @Override
    protected void processValidation(String[] personDataList) {
        String ldap = personDataList[personDataList.length - 1];
        boolean validLdap = ldap.startsWith("(") && ldap.endsWith(")");

        if (!validLdap) {
            throw new InvalidLdapException(personDataList);
        }
    }
}
