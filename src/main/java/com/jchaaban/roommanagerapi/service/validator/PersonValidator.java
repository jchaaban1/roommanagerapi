package com.jchaaban.roommanagerapi.service.validator;

import com.jchaaban.roommanagerapi.rest.exception.person.MissingPersonDataException;
import org.springframework.stereotype.Component;

@Component
public abstract class PersonValidator {
    private static final int MIN_PERSON_DATA_ELEMENTS = 3;
    public void validate(String[] personDataList) {

        if (personDataList.length < MIN_PERSON_DATA_ELEMENTS) {
            throw new MissingPersonDataException(personDataList);
        }

        processValidation(personDataList);
    }

    protected abstract void processValidation(String [] personDataList);
}
