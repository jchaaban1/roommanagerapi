package com.jchaaban.roommanagerapi.service.validator;

import com.jchaaban.roommanagerapi.rest.exception.room.InvalidRoomNumberException;
import org.springframework.stereotype.Component;

@Component
public class RoomNumberValidator {

    public static final String SPACE_CHARACTER = " ";

    public void validate(String roomNumber) {
        if (roomNumber.contains(SPACE_CHARACTER) || containsOnlyLetters(roomNumber) || roomNumber.length() != 4) {
            throw new InvalidRoomNumberException(roomNumber);
        }
    }

    private boolean containsOnlyLetters(String roomNumber) {
        return roomNumber.chars().allMatch(Character::isAlphabetic);
    }
}
