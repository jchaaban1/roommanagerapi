package com.jchaaban.roommanagerapi.service.validator;

import com.jchaaban.roommanagerapi.rest.exception.person.DuplicatedPersonException;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

@Component
public class UniquePersonValidator implements Validator {
    public static final String COMMA = ",";

    @Override
    public void validate(List<String> lines) throws DuplicatedPersonException {
        List<String> allLdaps = lines.stream().flatMap(this::extractLdap).toList();
        List<String> notUniqueLdaps = allLdaps.stream().filter(isNotUnique(allLdaps)).toList();
        if (!notUniqueLdaps.isEmpty()) {
            throw new DuplicatedPersonException(notUniqueLdaps);
        }
    }

    private Predicate<String> isNotUnique(List<String> allLdaps) {
        return ldap -> Collections.frequency(allLdaps, ldap) > 1;
    }

    private Stream<String> extractLdap(String line) {
        String[] person = line.split(COMMA);
        return Arrays.stream(Arrays.copyOfRange(person, 1, person.length));
    }
}
