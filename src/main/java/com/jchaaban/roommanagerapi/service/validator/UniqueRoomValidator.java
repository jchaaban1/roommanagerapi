package com.jchaaban.roommanagerapi.service.validator;

import com.jchaaban.roommanagerapi.rest.exception.room.DuplicatedRoomException;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UniqueRoomValidator implements Validator {
    public static final String COMMA = ",";

    public void validate(List<String> lines) throws DuplicatedRoomException {
        List<String> roomNumbers = lines
                .stream()
                .map(this::toRoomNumber)
                .toList();

        List<String> duplicatedRoomIds = roomNumbers
                .stream()
                .filter(roomId -> Collections.frequency(roomNumbers, roomId) > 1)
                .collect(Collectors.toList());

        if (!duplicatedRoomIds.isEmpty()) {
            throw new DuplicatedRoomException(duplicatedRoomIds);
        }
    }

    private String toRoomNumber(String line) {
        return line.split(COMMA)[0];
    }
}
