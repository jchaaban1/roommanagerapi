package com.jchaaban.roommanagerapi.service.validator;

import com.jchaaban.roommanagerapi.rest.exception.person.InvalidPersonDataException;

import java.util.List;

public interface Validator {
    void validate(List<String> lines) throws InvalidPersonDataException;
}
