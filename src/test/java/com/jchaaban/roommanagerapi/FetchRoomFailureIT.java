package com.jchaaban.roommanagerapi;

import com.jchaaban.roommanagerapi.rest.RoomManagementApiResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FetchRoomFailureIT {

    @Autowired
    TestRestTemplate restTemplate;

    public static Stream<Arguments> invalidRoomNumbersProvider() {
        return Stream.of(
                Arguments.of("11111"),
                Arguments.of("111")
        );
    }

    @BeforeEach
    public void setUp() {
        LinkedMultiValueMap<Object, Object> request = new LinkedMultiValueMap<>();
        request.add("file", new ClassPathResource("/csv/sitzplan.csv"));
        restTemplate.postForEntity("/api/import", request, String.class);
    }

    @ParameterizedTest
    @MethodSource("invalidRoomNumbersProvider")
    @DisplayName("When room number format is invalid, we should receive an error code 5")
    public void roomNumberInvalidFormat(String roomNumber) {
        ResponseEntity<RoomManagementApiResponse> response = restTemplate.getForEntity("/api/room/" + roomNumber, RoomManagementApiResponse.class);
        RoomManagementApiResponse apiResponse = response.getBody();

        assertThat(apiResponse).isNotNull();
        assertThat(apiResponse.getCode()).isEqualTo(6);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(apiResponse.getMessage()).isNotNull();

        assertThat(apiResponse.getMessage()).contains(
                "The room number: "
                        + roomNumber
                        + " is invalid, "
                + "the room number must be exactly four characters long, don't contain any spaces and should contain numbers"
        );
    }

    @Test
    @DisplayName("Fetching a room that don't exists")
    public void roomNotFound() {
        String roomNumber = "9999";
        ResponseEntity<RoomManagementApiResponse> response = restTemplate.getForEntity("/api/room/" + roomNumber, RoomManagementApiResponse.class);
        RoomManagementApiResponse apiResponse = response.getBody();

        assertThat(apiResponse).isNotNull();
        assertThat(apiResponse.getCode()).isEqualTo(5);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(apiResponse.getMessage()).isNotNull();
        assertThat(apiResponse.getMessage()).contains("The room having number: " + roomNumber + " does not exist");
    }


}
