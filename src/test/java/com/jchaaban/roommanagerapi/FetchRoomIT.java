package com.jchaaban.roommanagerapi;

import com.jchaaban.roommanagerapi.entity.Room;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FetchRoomIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @BeforeEach
    public void setUp() {
        LinkedMultiValueMap<Object, Object> request = new LinkedMultiValueMap<>();
        request.add("file", new ClassPathResource("/csv/sitzplan.csv"));
        restTemplate.postForEntity("/api/import", request, String.class);
    }

    @Test
    @DisplayName("There is an endpoint api/room/{roomNumber} that fetches a room having number equals to {roomNumber}")
    public void sendRequestToFetchRoom() {
        ResponseEntity<Room> response = restTemplate.getForEntity("/api/room/1111", Room.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody()).isInstanceOf(Room.class);
    }

    @Test
    @DisplayName("There is an endpoint api/room/{roomNumber} that fetches all rooms when no room number is given")
    public void fetchAllRooms() {
        ResponseEntity<List> response = restTemplate.getForEntity("/api/room", List.class);
        List rooms = response.getBody();
        assertThat(rooms).isNotNull();
        assertThat(rooms.size()).isEqualTo(15);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}
