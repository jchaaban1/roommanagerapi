package com.jchaaban.roommanagerapi;

import com.jchaaban.roommanagerapi.entity.Person;
import com.jchaaban.roommanagerapi.entity.Room;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FetchRoomSuccessIT {

    @Autowired
    TestRestTemplate restTemplate;

    @BeforeEach
    public void setUp() {
        LinkedMultiValueMap<Object, Object> request = new LinkedMultiValueMap<>();
        request.add("file", new ClassPathResource("/csv/validRooms.csv"));
        restTemplate.postForEntity("/api/import", request, String.class);
    }

    @Test
    @DisplayName("Fetching existing room with persons will return a room object")
    public void fetchExistingRoom() {
        String roomNumber = "1234";

        List<Person> expectedPersonsInRoom = List.of(
                new Person("", "Michael", "", "Brown", "mbrown"),
                new Person("Dr.", "Emily", "", "Davis", "edavis")
        );

        ResponseEntity<Room> response = restTemplate.getForEntity("/api/room/" + roomNumber, Room.class);

        Room room = response.getBody();

        assertThat(room).isNotNull();
        assertThat(room.getRoomNumber()).isEqualTo(roomNumber);
        assertThat(room.getPeople().size()).isEqualTo(expectedPersonsInRoom.size());

        expectedPersonsInRoom.forEach(expectedPerson ->
                assertThat(room.getPeople()).anySatisfy(actualPerson -> {
                    assertThat(actualPerson.getTitle()).isEqualTo(expectedPerson.getTitle());
                    assertThat(actualPerson.getFirstName()).isEqualTo(expectedPerson.getFirstName());
                    assertThat(actualPerson.getNameAddition()).isEqualTo(expectedPerson.getNameAddition());
                    assertThat(actualPerson.getLastname()).isEqualTo(expectedPerson.getLastname());
                    assertThat(actualPerson.getLdap()).isEqualTo(expectedPerson.getLdap());
                })
        );

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }


    @Test
    @DisplayName("When no room number given then return all rooms")
    public void fetchAllRooms() {
        ResponseEntity<List<Room>> response = restTemplate.exchange(
                "/api/room",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() { }
        );

        List<Room> rooms = response.getBody();

        assertThat(rooms).isNotNull();
        assertThat(rooms).isNotNull();
        assertThat(rooms).isNotEmpty();
        assertThat(rooms).hasSize(2);
        assertThat(rooms).extracting(Room::getRoomNumber).contains("1234", "1235");

    }
}
