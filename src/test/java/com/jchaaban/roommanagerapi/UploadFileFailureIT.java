package com.jchaaban.roommanagerapi;

import com.jchaaban.roommanagerapi.rest.RoomManagementApiResponse;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UploadFileFailureIT {

    @Autowired
    TestRestTemplate restTemplate;

    private ResponseEntity<RoomManagementApiResponse> sendRequestAndReturnResponse(String filename) {
        LinkedMultiValueMap<Object, Object> request = new LinkedMultiValueMap<>();
        request.add("file", new ClassPathResource(filename));
        return restTemplate.postForEntity("/api/import", request, RoomManagementApiResponse.class);
    }

    private void assertResponse(ResponseEntity<RoomManagementApiResponse> response, int errorCode, String errorMessage) {
        RoomManagementApiResponse apiResponse = response.getBody();

        assertThat(apiResponse).isNotNull();
        assertThat(apiResponse.getCode()).isEqualTo(errorCode);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(apiResponse.getMessage()).isNotNull();
        assertThat(apiResponse.getMessage()).contains(errorMessage);
    }

    @Test
    @DisplayName("Room Duplications are not allowed, otherwise error code 2 (HTTP 400)")
    void roomNotUnique() {
        assertResponse(sendRequestAndReturnResponse("/csv/roomNotUnique.csv"), 2, "The following room numbers are duplicated: ");
    }

    @Test
    @DisplayName("Person Duplications are not allowed, otherwise error code 3 (HTTP 400)")
    void personNotUnique() {
        assertResponse(sendRequestAndReturnResponse("/csv/failure-duplicated-persons.csv"), 3, "The following persons are not unique: ");
    }

    @Test
    @DisplayName("No file argument given, should return error code and message")
    void missingFileArgument() {
        LinkedMultiValueMap<Object, Object> request = new LinkedMultiValueMap<>();
        ResponseEntity<RoomManagementApiResponse> response = restTemplate.postForEntity("/api/import", request, RoomManagementApiResponse.class);

        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getMessage()).isEqualTo("No file was uploaded");
    }

    public static Stream<Arguments> invalidPersonDataProvider() {
        return Stream.of(
                Arguments.of("/csv/failure - missing-person-data.csv", 4, "Missing Person data in:"),
                Arguments.of("/csv/failure - invalid-ldap-data.csv", 4, "Invalid Ldap in: "),
                Arguments.of("/csv/failure - invalid-lastname-data.csv", 4, "Invalid lastname in: "),
                Arguments.of("/csv/failure - invalid-firstname-equals-title-data.csv", 4, "Invalid firstname in: "),
                Arguments.of("/csv/failure - invalid-firstname-equals-name-addition-data.csv", 4, "Invalid firstname in: "),
                Arguments.of("/csv/failure - invalid-firstname-equals-name-addition-when-title-exists-data.csv", 4, "Invalid firstname in: ")
        );
    }

    @ParameterizedTest
    @MethodSource("invalidPersonDataProvider")
    @DisplayName("Person data must contain specific elements and format")
    void runInvalidPersonDataTests(String filename, int errorCode, String errorMessage) {
        assertResponse(sendRequestAndReturnResponse(filename), errorCode, errorMessage);
    }
    public static Stream<Arguments> invalidRoomNumberProvider() {
        return Stream.of(
                Arguments.of(
                        "/csv/failure-invalidRoomNumberDoesNotExist.csv",
                        6,
                        "the room number must be exactly four characters long, don't contain any spaces and should contain numbers"
                ),
                Arguments.of(
                        "/csv/failure-invalidRoomNumberTooLong.csv",
                        6,
                        "the room number must be exactly four characters long, don't contain any spaces and should contain numbers"
                ),
                Arguments.of(
                        "/csv/failure-invalidRoomNumberTooShort.csv",
                        6,
                        "the room number must be exactly four characters long, don't contain any spaces and should contain numbers"
                )
        );
    }

    @ParameterizedTest
    @MethodSource("invalidRoomNumberProvider")
    @DisplayName("Room number must be exactly four characters long, don't have to contain any spaces and should contain numbers")
    void runInvalidRoomNumberTests(String filename, int errorCode, String errorMessage) {
        assertResponse(sendRequestAndReturnResponse(filename), errorCode, errorMessage);
    }

}
