package com.jchaaban.roommanagerapi;

import com.jchaaban.roommanagerapi.entity.Room;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UploadFileIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    @DisplayName("There is an endpoint api/import that takes a file as a parameter")
    void sendRequestToImportFile() {
        LinkedMultiValueMap<Object, Object> request = new LinkedMultiValueMap<>();
        request.add("file", new ClassPathResource("/csv/sitzplan.csv"));
        ResponseEntity<Void> response = restTemplate.postForEntity("/api/import", request, Void.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @DisplayName("New file uploads overides old data saved by the previous upload")
    public void newFileUploadOverridesOldUploadData() {
        uploadFile("csv/sitzplan.csv");
        List<Room> oldRooms = fetchRooms();
        uploadFile("csv/validRoom.csv");
        List<Room> newRooms = fetchRooms();
        assertThat(newRooms).size().isEqualTo(1);
        assertThat(oldRooms).size().isNotEqualTo(newRooms);
    }

    private List<Room> fetchRooms() {
        ResponseEntity<List> response = restTemplate.getForEntity("/api/room", List.class);
        return response.getBody();
    }

    private void uploadFile(String filename) {
        LinkedMultiValueMap<Object, Object> request = new LinkedMultiValueMap<>();
        request.add("file", new ClassPathResource(filename));
        restTemplate.postForEntity("/api/import", request, String.class);
    }


}
