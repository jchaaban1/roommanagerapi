package com.jchaaban.roommanagerapi;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UploadFileSuccessIT {

    @Autowired
    TestRestTemplate restTemplate;

    private ResponseEntity<String> sendRequestAndReturnResponse(String filename) {
        LinkedMultiValueMap<Object, Object> request = new LinkedMultiValueMap<>();
        request.add("file", new ClassPathResource(filename));
        return restTemplate.postForEntity("/api/import", request, String.class);
    }

    public static Stream<Arguments> validPersonDataProvider() {
        return Stream.of(
                Arguments.of("/csv/validRoom.csv"),
                Arguments.of("/csv/validRooms.csv"),
                Arguments.of("/csv/sitzplan.csv")
        );
    }

    @ParameterizedTest
    @MethodSource("validPersonDataProvider")
    @DisplayName("File is successfully uploaded")
    void fileIsSuccessfullyUploaded(String filename) {
        ResponseEntity<String> response = sendRequestAndReturnResponse(filename);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        String successMessage = response.getBody();
        assertThat(successMessage).isNotNull();
        assertThat(successMessage).contains("The file was uploaded successfully");
    }
}
