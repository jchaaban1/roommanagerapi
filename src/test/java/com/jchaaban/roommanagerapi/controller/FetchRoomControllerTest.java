package com.jchaaban.roommanagerapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jchaaban.roommanagerapi.entity.Person;
import com.jchaaban.roommanagerapi.entity.Room;
import com.jchaaban.roommanagerapi.repository.RoomRepository;
import com.jchaaban.roommanagerapi.rest.RoomManagementApiResponse;
import com.jchaaban.roommanagerapi.rest.controller.FetchRoomController;
import com.jchaaban.roommanagerapi.rest.exception.room.InvalidRoomNumberException;
import com.jchaaban.roommanagerapi.rest.exception.room.RoomNotFoundException;
import com.jchaaban.roommanagerapi.service.FetchRoomService;
import com.jchaaban.roommanagerapi.service.FileImportService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(FetchRoomController.class)
public class FetchRoomControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FetchRoomService fetchRoomService;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @MockBean
    private RoomRepository roomRepositoryMock;

    @MockBean
    private FileImportService fileImportService;

    @Test
    @DisplayName("Fetch all rooms successfully")
    public void fetchAllRoomsSuccessfully() throws Exception {
        Room room1234 = new Room("1234");

        room1234.addPerson(new Person("", "Franz", "Mustermann", "", "fmustermann"));
        room1234.addPerson(new Person("Dr.", "Brigitte", "Müller", "", "bmueller"));

        Room room1235 = new Room("1235");
        room1235.addPerson(new Person("", "Erika", "Mustermann", "", "emustermann"));
        room1235.addPerson(new Person("", "Joseph", "von Klippstein", "", "jklippstein"));

        List<Room> rooms = List.of(room1234, room1235);
        Mockito.when(fetchRoomService.fetchAllRooms()).thenReturn(rooms);

        mockMvc.perform(get("/api/room")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(rooms)));
    }

    @Test
    @DisplayName("Fetch room successfully")
    public void fetchRoomSuccessfully() throws Exception {
        Room firstRoom = new Room("1234");
        firstRoom.addPerson(new Person("", "Franz", "Mustermann", "", "fmustermann"));
        firstRoom.addPerson(new Person("Dr.", "Brigitte", "Müller", "", "bmueller"));

        Mockito.when(fetchRoomService.fetchRoom("1234")).thenReturn(firstRoom);

        mockMvc.perform(get("/api/room/{number}", "1234")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(firstRoom)));
    }

    @Test
    @DisplayName("Fetch room having invalid room number format throws InvalidRoomNumberException")
    public void fetchRoomHavingInvalidRoomNumberFormat() throws Exception {
        String invalidRoomNumber = "1";
        Mockito.when(fetchRoomService.fetchRoom(invalidRoomNumber))
                .thenThrow(new InvalidRoomNumberException(invalidRoomNumber));

        RoomManagementApiResponse expectedResponse = new RoomManagementApiResponse(
                "The room number: " + invalidRoomNumber + " is invalid, the room number must be exactly four "
                        + "characters long, don't contain any spaces and should contain numbers",
                FetchRoomController.INVALID_ROOM_NUMBER_FORMAT
        );

        mockMvc.perform(get("/api/room/{number}", invalidRoomNumber)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(expectedResponse)));
    }

    @Test
    @DisplayName("Fetch not existing room throws RoomNotFoundException")
    public void fetchNotExistingRoom() throws Exception {
        String nonExistentRoomNumber = "9999";
        Mockito.when(fetchRoomService.fetchRoom(nonExistentRoomNumber))
                .thenThrow(new RoomNotFoundException(nonExistentRoomNumber));

        RoomManagementApiResponse expectedResponse = new RoomManagementApiResponse(
                "The room having number: "
                        + nonExistentRoomNumber
                        + " does not exist",
                FetchRoomController.ROOM_NOT_FOUND
        );

        mockMvc.perform(get("/api/room/{number}", nonExistentRoomNumber)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().json(objectMapper.writeValueAsString(expectedResponse)));
    }



}
