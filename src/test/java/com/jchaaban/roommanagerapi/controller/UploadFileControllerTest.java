package com.jchaaban.roommanagerapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jchaaban.roommanagerapi.rest.RoomManagementApiResponse;
import com.jchaaban.roommanagerapi.rest.controller.UploadFileController;
import com.jchaaban.roommanagerapi.rest.exception.person.*;
import com.jchaaban.roommanagerapi.rest.exception.room.DuplicatedRoomException;
import com.jchaaban.roommanagerapi.rest.exception.room.InvalidRoomNumberException;
import com.jchaaban.roommanagerapi.service.FileImportService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UploadFileController.class)
class UploadFileControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FileImportService fileImportService;

    private final ObjectMapper objectMapper = new ObjectMapper();

    public static Stream<Arguments> validDataProvider() {
        return Stream.of(
                Arguments.of("1111"),
                Arguments.of("1111 Jaafar Chaaban (jchaaban)"),
                Arguments.of("1111 Dr. Tom Chaaban (tchaaban)"),
                Arguments.of("1111 Dr. Tom von Chaaban (tchaaban)"),
                Arguments.of("1111, Dr. Tom de Chaaban (tchaaban)"),
                Arguments.of("1111, Tom Peter Chaaban (tchaaban)"),
                Arguments.of("1111, Dr. Tom Peter Chaaban (tchaaban)"),
                Arguments.of(
                        "1111, Tom Peter von Chaaban (tchaaban)"
                                + System.lineSeparator()
                                + "1112, Marcel von Vogel (mvogel)"
                )
        );
    }

    @ParameterizedTest
    @MethodSource("validDataProvider")
    @DisplayName("Uploading file with valid data leads to successful upload")
    public void importFileValidDataSuccessfully(String fileContent) throws Exception {
        MockMultipartFile file = new MockMultipartFile("file", fileContent.getBytes());

        mockMvc.perform(multipart("/api/import")
                        .file(file)
                        .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(status().isOk())
                .andExpect(content().string("The file was uploaded successfully"));
    }

    @Test
    public void throwsFileArgumentNotFoundException() throws Exception {
        mockMvc.perform(multipart("/api/import")
                        .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(status().isBadRequest())
                .andExpect(result -> {
                    content().string("No file was uploaded");
                });
    }

    @Test
    @DisplayName("Uploading file with duplicated room number throws DuplicateRoomException")
    public void throwsDuplicateRoomException() throws Exception {
        String duplicatedRoomNumber = "1111";
        String fileContent = duplicatedRoomNumber + ",,,," + System.lineSeparator() + duplicatedRoomNumber + ",,,,";

        MockMultipartFile file = new MockMultipartFile("file", fileContent.getBytes());


        List<String> duplicatedRooms = List.of("1111");

        RoomManagementApiResponse expectedResponse = new RoomManagementApiResponse(
                "The following room numbers are duplicated: " + String.join(",", duplicatedRooms),
                UploadFileController.DUPLICATE_ROOM_ERROR_CODE
        );

        Mockito.doThrow(new DuplicatedRoomException(duplicatedRooms))
                .when(fileImportService).execute(Mockito.anyString());

        mockMvc.perform(multipart("/api/import")
                        .file(file)
                        .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(expectedResponse)));
    }

    @Test
    @DisplayName("Uploading file with invalid room number throws InvalidRoomNumberException")
    public void throwsInvalidRoomNumberException() throws Exception {
        String invalidRoomNumber = "111";
        String fileContent = invalidRoomNumber + ", Jaafar Chaaban (jchaaban)";

        MockMultipartFile file = new MockMultipartFile(
                "file",
                fileContent.getBytes()
        );

        RoomManagementApiResponse expectedResponse = new RoomManagementApiResponse(
                "The room number: " + invalidRoomNumber + " is invalid, the room number must be exactly four"
                        + " characters long, don't contain any spaces and should contain numbers",
                UploadFileController.INVALID_ROOM_NUMBER_FORMAT
        );


        Mockito.doThrow(new InvalidRoomNumberException(invalidRoomNumber))
                .when(fileImportService).execute(Mockito.anyString());

        mockMvc.perform(multipart("/api/import")
                        .file(file)
                        .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(expectedResponse)));
    }

    @Test
    @DisplayName("Uploading file with duplicated person data throws InvalidLdapException")
    public void throwsDuplicatedPersonException() throws Exception {
        String duplicatedLdap = "(jchaaban)";
        String fileContent = "1111, Jaafar Chaaban " + duplicatedLdap + ", Jaafar Chaaban " + duplicatedLdap;

        MockMultipartFile file = new MockMultipartFile(
                "file",
                fileContent.getBytes()
        );

        List<String> duplicatedLdapsList = List.of(duplicatedLdap);

        RoomManagementApiResponse expectedResponse = new RoomManagementApiResponse(
                "The following persons are not unique: " + String.join(",", duplicatedLdapsList),
                (UploadFileController.DUPLICATE_PERSON_ERROR_CODE)
        );

        Mockito.doThrow(new DuplicatedPersonException(duplicatedLdapsList))
                .when(fileImportService).execute(Mockito.anyString());

        mockMvc.perform(multipart("/api/import")
                        .file(file)
                        .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(expectedResponse)));
    }

    @Test
    @DisplayName("Uploading file with invalid LDAP throws InvalidLdapException")
    public void throwsInvalidLdapException() throws Exception {
        String fileContent = "1111 Chaaban jchaaban";
        String[] personDataList = {"Jaafar", "Chaaban", "jchaaban"};
        String errorMessage = "Invalid Ldap in: " + String.join(" ", personDataList);

        MockMultipartFile file = new MockMultipartFile(
                "file",
                fileContent.getBytes()
        );

        InvalidPersonDataException exceptionToThrow = new InvalidLdapException(personDataList);

        RoomManagementApiResponse expectedResponse = new RoomManagementApiResponse(
                errorMessage,
                UploadFileController.INVALID_PERSON_ERROR_CODE
        );

        Mockito.doThrow(exceptionToThrow)
                .when(fileImportService).execute(Mockito.anyString());

        mockMvc.perform(multipart("/api/import")
                        .file(file)
                        .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(expectedResponse)));
    }

    @Test
    @DisplayName("Uploading file with invalid lastname throws InvalidLastnameException")
    public void throwsInvalidLastnameException() throws Exception {
        String fileContent = "1111 Jaafar von (tvon)";
        String[] personDataList = {"Jaafar", "von", "(tvon)"};
        String errorMessage = "Invalid lastname in: " + String.join(" ", personDataList);

        MockMultipartFile file = new MockMultipartFile(
                "file",
                fileContent.getBytes()
        );

        InvalidPersonDataException exceptionToThrow = new InvalidLastnameException(personDataList);

        RoomManagementApiResponse expectedResponse = new RoomManagementApiResponse(
                errorMessage,
                UploadFileController.INVALID_PERSON_ERROR_CODE
        );

        Mockito.doThrow(exceptionToThrow)
                .when(fileImportService).execute(Mockito.anyString());

        mockMvc.perform(multipart("/api/import")
                        .file(file)
                        .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(expectedResponse)));
    }

    @Test
    @DisplayName("Uploading file with invalid firstname throws InvalidFirstnameException")
    public void throwsInvalidFirstnameException() throws Exception {
        String fileContent = "1111 von (tchaaban)";
        String[] personDataList = {"von", "(tchaaban)"};
        String errorMessage = "Invalid firstname in: " + String.join(" ", personDataList);

        MockMultipartFile file = new MockMultipartFile(
                "file",
                fileContent.getBytes()
        );

        InvalidPersonDataException exceptionToThrow = new InvalidFirstnameException(personDataList);

        RoomManagementApiResponse expectedResponse = new RoomManagementApiResponse(
                errorMessage,
                UploadFileController.INVALID_PERSON_ERROR_CODE
        );

        Mockito.doThrow(exceptionToThrow)
                .when(fileImportService).execute(Mockito.anyString());

        mockMvc.perform(multipart("/api/import")
                        .file(file)
                        .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(expectedResponse)));
    }

    @Test
    @DisplayName("Uploading file with missing person data throws MissingPersonDataException")
    public void throwsMissingPersonDataException() throws Exception {
        String fileContent = "1111 (tchaaban)";
        String[] personDataList = {"(tchaaban)"};
        String errorMessage = "Missing Person data in: " + String.join(" ", personDataList);

        MockMultipartFile file = new MockMultipartFile(
                "file",
                fileContent.getBytes()
        );

        InvalidPersonDataException exceptionToThrow = new MissingPersonDataException(personDataList);

        RoomManagementApiResponse expectedResponse = new RoomManagementApiResponse(
                errorMessage,
                UploadFileController.INVALID_PERSON_ERROR_CODE
        );

        Mockito.doThrow(exceptionToThrow)
                .when(fileImportService).execute(Mockito.anyString());

        mockMvc.perform(multipart("/api/import")
                        .file(file)
                        .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(expectedResponse)));
    }

}