package com.jchaaban.roommanagerapi.exceptionHandler;

import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class HTTPMethodNotAllowedExceptionHandlerTest {

    @InjectMocks
    private HTTPMethodNotAllowedExceptionHandler exceptionHandler;

    @Mock
    private HttpServletRequest request;

    @Test
    @DisplayName("Handle Method Not Supported Exception")
    public void handleMethodNotSupportedException() {
        HttpRequestMethodNotSupportedException exception = new HttpRequestMethodNotSupportedException("POST");
        ResponseEntity<Map<String, Object>> response = exceptionHandler.handleMethodNotSupportedException(exception, request);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.METHOD_NOT_ALLOWED);

        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().get("code")).isEqualTo(HttpStatus.METHOD_NOT_ALLOWED.value());
        assertThat(response.getBody().get("message").toString()).contains("POST");
    }

}
