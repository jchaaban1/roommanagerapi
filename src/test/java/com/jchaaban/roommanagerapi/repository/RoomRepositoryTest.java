package com.jchaaban.roommanagerapi.repository;

import com.jchaaban.roommanagerapi.entity.Room;
import com.jchaaban.roommanagerapi.rest.exception.room.RoomNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class RoomRepositoryTest {

    private RoomRepository roomRepositoryToTest;

    @BeforeEach
    public void setUp() {
        roomRepositoryToTest = new RoomRepository();
        roomRepositoryToTest.add(new Room("1111"));
    }

    @Test
    @DisplayName("The fetchRoom method of the RoomRepository throws an RoomNotFoundException when non existing room is fetched")
    public void fetchRoomThrowsExceptionWhenNonExistingRoomIsFetched() {
        assertThatThrownBy(() -> roomRepositoryToTest.fetchRoom("2222")).isInstanceOf(RoomNotFoundException.class);
    }

    @Test
    @DisplayName("The fetchRoom method of the RoomRepository does not throw an RoomNotFoundException when an existing room is fetched")
    public void fetchRoomDoesNotThrowExceptionWhenExistingRoomIsFetched() {
        assertDoesNotThrow(() -> roomRepositoryToTest.fetchRoom("1111"));
    }

    @Test
    @DisplayName("The fetchRoom method of the RoomRepository does not throw an RoomNotFoundException when an existing room is fetched")
    public void fetchRoomsReturnsAListOfRooms() {
        List<Room> rooms = roomRepositoryToTest.fetchAllRooms();
        assertThat(rooms).isNotNull();
        assertThat(rooms).size().isEqualTo(1);
        assertThat(rooms).extracting(Room::getRoomNumber).contains("1111");
    }


}