package com.jchaaban.roommanagerapi.service;

import com.jchaaban.roommanagerapi.entity.Room;
import com.jchaaban.roommanagerapi.repository.RoomRepository;
import com.jchaaban.roommanagerapi.rest.exception.room.InvalidRoomNumberException;
import com.jchaaban.roommanagerapi.rest.exception.room.RoomNotFoundException;
import com.jchaaban.roommanagerapi.service.validator.RoomNumberValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class FetchRoomServiceTest {

    private FetchRoomService fetchRoomServiceToTest;

    @Mock
    private RoomRepository roomRepository;
    @Mock
    private RoomNumberValidator roomNumberValidator;

    @BeforeEach
    void setUp() {
        fetchRoomServiceToTest = new FetchRoomService(roomRepository, roomNumberValidator);
    }

    @Test
    @DisplayName("Fetch room Successfully")
    void fetchRoomSuccessfully() throws RoomNotFoundException {
        String roomNumber = "1111";
        Room expectedRoom = new Room(roomNumber);
        when(roomRepository.fetchRoom(anyString())).thenReturn(expectedRoom);

        Room actualRoom = fetchRoomServiceToTest.fetchRoom(roomNumber);

        assertEquals(expectedRoom, actualRoom);
        verify(roomNumberValidator, times(1)).validate(roomNumber);
        verify(roomRepository, times(1)).fetchRoom(roomNumber);
    }

    @Test
    @DisplayName("Fetch all rooms successfully")
    void fetchAllRoomsSuccessfully() {
        Room room1 = new Room("1001");
        Room room2 = new Room("1002");
        List<Room> expectedRooms = Arrays.asList(room1, room2);
        when(roomRepository.fetchAllRooms()).thenReturn(expectedRooms);

        List<Room> actualRooms = fetchRoomServiceToTest.fetchAllRooms();

        assertEquals(expectedRooms, actualRooms);
        verify(roomRepository, times(1)).fetchAllRooms();
    }

    @Test
    @DisplayName("Fetch not existing room throws RoomNotFoundException")
    void fetchNotExistingRoomThrowsRoomNotFoundException() throws RoomNotFoundException {
        String roomNumber = "9999";
        when(roomRepository.fetchRoom(anyString())).thenThrow(new RoomNotFoundException(roomNumber));

        assertThrows(RoomNotFoundException.class, () -> fetchRoomServiceToTest.fetchRoom(roomNumber));

        verify(roomNumberValidator, times(1)).validate(roomNumber);
        verify(roomRepository, times(1)).fetchRoom(roomNumber);
    }

    @Test
    @DisplayName("Fetch room with invalid format throws InvalidRoomNumberException")
    void fetchRoomWithInvalidFormatThrowsInvalidRoomNumberException() throws InvalidRoomNumberException {
        String roomNumber = "99a9";
        doThrow(new InvalidRoomNumberException(roomNumber)).when(roomNumberValidator).validate(anyString());

        assertThrows(InvalidRoomNumberException.class, () -> fetchRoomServiceToTest.fetchRoom(roomNumber));

        verify(roomNumberValidator, times(1)).validate(roomNumber);
    }

}