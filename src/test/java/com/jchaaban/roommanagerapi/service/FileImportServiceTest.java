package com.jchaaban.roommanagerapi.service;

import com.jchaaban.roommanagerapi.repository.RoomRepository;
import com.jchaaban.roommanagerapi.rest.exception.person.DuplicatedPersonException;
import com.jchaaban.roommanagerapi.rest.exception.room.DuplicatedRoomException;
import com.jchaaban.roommanagerapi.service.extractor.RoomExtractorService;
import com.jchaaban.roommanagerapi.service.validator.Validator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class FileImportServiceTest {

    private FileImportService fileImportServiceToTest;

    @Mock
    private RoomRepository roomRepositoryMock;
    @Mock
    private RoomExtractorService roomExtractorServiceMock;
    @Mock
    private Validator uniqueRoomValidatorMock;
    @Mock
    private Validator uniquePersonValidatorMock;

    @BeforeEach
    void setUp() {
        List<Validator> validators = List.of(uniqueRoomValidatorMock, uniquePersonValidatorMock);
        fileImportServiceToTest = new FileImportService(roomRepositoryMock, roomExtractorServiceMock, validators);
    }


    private String createFileContent(String... lines) {
        return String.join(System.lineSeparator(), lines);
    }

    @Test
    @DisplayName("Successfully importing file having no errors")
    void successfully() throws DuplicatedRoomException, DuplicatedPersonException {
        String fileContent = createFileContent(
                "1111,Jaafar Chaaban,(jchaaban)", "1112,Tom Puschmann,(tpuschmann)"
        );

        doNothing().when(uniqueRoomValidatorMock).validate(any());
        doNothing().when(uniquePersonValidatorMock).validate(any());

        fileImportServiceToTest.execute(fileContent);

        verify(uniqueRoomValidatorMock, times(1)).validate(any());
        verify(uniquePersonValidatorMock, times(1)).validate(any());
        verify(roomExtractorServiceMock, times(2)).toRoom(anyString());
        verify(roomRepositoryMock, times(1)).reset();
        verify(roomRepositoryMock, times(2)).add(any());
    }

    @Test
    @DisplayName("Importing file having duplicated room throws DuplicatedRoomException")
    void throwsDuplicatedRoomException() {
        String fileContent = createFileContent(
                "1111,Jaafar Chaaban,(jchaaban)", "1111,Tom Puschmann,(tpuschmann)"
        );

        doThrow(new DuplicatedRoomException(List.of("1111"))).when(uniqueRoomValidatorMock).validate(any());

        assertThrows(DuplicatedRoomException.class, () -> fileImportServiceToTest.execute(fileContent));

        verify(uniqueRoomValidatorMock, times(1)).validate(any());
        verify(uniquePersonValidatorMock, never()).validate(any());
        verify(roomRepositoryMock, never()).reset();
        verify(roomExtractorServiceMock, never()).toRoom(anyString());
        verify(roomRepositoryMock, never()).add(any());
    }

    @Test
    @DisplayName("Importing file having duplicated person throws DuplicatedPersonException")
    void throwsDuplicatedPersonException() {
        String fileContent = createFileContent(
                "1111,Jaafar Chaaban,(jchaaban)", "1112,Jaafar Chaaban,(jchaaban)"
        );

        doNothing().when(uniqueRoomValidatorMock).validate(any());
        doThrow(new DuplicatedPersonException(List.of("(jchaaban)"))).when(uniquePersonValidatorMock).validate(any());

        assertThrows(DuplicatedPersonException.class, () -> fileImportServiceToTest.execute(fileContent));

        verify(uniqueRoomValidatorMock, times(1)).validate(any());
        verify(uniquePersonValidatorMock, times(1)).validate(any());
        verify(roomRepositoryMock, never()).reset();
        verify(roomExtractorServiceMock, never()).toRoom(anyString());
        verify(roomRepositoryMock, never()).add(any());
    }

}