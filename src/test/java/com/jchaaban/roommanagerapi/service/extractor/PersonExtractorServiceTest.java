package com.jchaaban.roommanagerapi.service.extractor;

import com.jchaaban.roommanagerapi.entity.Person;
import com.jchaaban.roommanagerapi.rest.exception.person.InvalidFirstnameException;
import com.jchaaban.roommanagerapi.rest.exception.person.InvalidLdapException;
import com.jchaaban.roommanagerapi.service.validator.FirstnameValidator;
import com.jchaaban.roommanagerapi.service.validator.LastnameValidator;
import com.jchaaban.roommanagerapi.service.validator.LdapValidator;
import com.jchaaban.roommanagerapi.service.validator.PersonValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PersonExtractorServiceTest {

    @Mock
    private FirstnameValidator firstnameValidator;
    @Mock
    private LastnameValidator lastnameValidator;
    @Mock
    private LdapValidator ldapValidator;

    private PersonExtractorService personExtractorServiceToTest;


    @BeforeEach
    public void setUp() {
        List<PersonValidator> personValidators = List.of(firstnameValidator, lastnameValidator, ldapValidator);
        personExtractorServiceToTest = new PersonExtractorService(personValidators);
    }

    @Test
    @DisplayName("Person having all fields")
    public void personHavingAllFields() {
        String input = "Dr. Jan von Hussmann (jhussmann)";
        Person expectedPerson = new Person("Dr.", "Jan", "von", "Hussmann", "jhussmann");

        Person actualPerson = personExtractorServiceToTest.toPerson(input);

        assertThat(expectedPerson.getTitle()).isEqualTo(actualPerson.getTitle());
        assertThat(expectedPerson.getFirstName()).isEqualTo(actualPerson.getFirstName());
        assertThat(expectedPerson.getLastname()).isEqualTo(actualPerson.getLastname());
        assertThat(expectedPerson.getLdap()).isEqualTo(actualPerson.getLdap());
        assertThat(expectedPerson.getNameAddition()).isEqualTo(actualPerson.getNameAddition());

        String[] personDataList = new String[]{"Dr.", "Jan", "von", "Hussmann", "(jhussmann)"};

        verify(firstnameValidator, times(1)).validate(personDataList);
        verify(lastnameValidator, times(1)).validate(personDataList);
        verify(ldapValidator, times(1)).validate(personDataList);
    }

    @Test
    @DisplayName("Person having no title")
    public void personHavingNoTitle() {
        String input = "Jan von Hussmann (jhussmann)";
        Person expectedPerson = new Person("", "Jan", "von", "Hussmann", "jhussmann");

        Person actualPerson = personExtractorServiceToTest.toPerson(input);

        assertThat(expectedPerson.getTitle()).isEqualTo(actualPerson.getTitle());
        assertThat(expectedPerson.getFirstName()).isEqualTo(actualPerson.getFirstName());
        assertThat(expectedPerson.getLastname()).isEqualTo(actualPerson.getLastname());
        assertThat(expectedPerson.getLdap()).isEqualTo(actualPerson.getLdap());
        assertThat(expectedPerson.getNameAddition()).isEqualTo(actualPerson.getNameAddition());

        String[] personDataList = new String[]{"Jan", "von", "Hussmann", "(jhussmann)"};

        verify(firstnameValidator, times(1)).validate(personDataList);
        verify(lastnameValidator, times(1)).validate(personDataList);
        verify(ldapValidator, times(1)).validate(personDataList);
    }

    @Test
    @DisplayName("Person with No Firstname, with Title with Name Addition throws InvalidFirstnameException")
    public void personWithInvalidFirstnameWithTitleWithNameAdditionThrowsInvalidFirstnameException() {
        String input = "Dr. von Hussmann (jhussmann)";
        doThrow(new InvalidFirstnameException(new String[]{"Dr.", "von", "Hussmann", "(jhussmann)"}))
                .when(firstnameValidator).validate(any(String[].class));

        assertThrows(InvalidFirstnameException.class, () -> personExtractorServiceToTest.toPerson(input));
    }

    @Test
    @DisplayName("Person with No Firstname, No Title with Name Addition throws InvalidFirstnameException")
    public void personWithNoFirstnameNoTitleWithNameAdditionThrowsInvalidFirstnameException() {
        String input = "von Hussmann (jhussmann)";
        doThrow(new InvalidFirstnameException(new String[]{"von", "Hussmann", "(jhussmann)"}))
                .when(firstnameValidator).validate(any(String[].class));

        assertThrows(InvalidFirstnameException.class, () -> personExtractorServiceToTest.toPerson(input));
    }

    @Test
    @DisplayName("Person with No Firstname, No Title, No Name Addition throws InvalidFirstnameException")
    public void personWithNoFirstnameFirstnameNoTitleNoNameAdditionThrowsInvalidFirstnameException() {
        String input = "Hussmann (jhussmann)";
        doThrow(new InvalidFirstnameException(new String[]{"Hussmann", "(jhussmann)"}))
                .when(firstnameValidator).validate(any(String[].class));

        assertThrows(InvalidFirstnameException.class, () -> personExtractorServiceToTest.toPerson(input));
    }

    @Test
    @DisplayName("Person with Invalid Ldap throws InvalidLdapException")
    public void personWithInvalidLdapThrowsInvalidLdapException() {
        String input = "Dr. Jan von Hussmann jhussmann";
        doThrow(new InvalidLdapException(new String[]{"Dr.", "Jan", "von", "Hussmann", "jhussmann"}))
                .when(ldapValidator).validate(any(String[].class));

        assertThrows(InvalidLdapException.class, () -> personExtractorServiceToTest.toPerson(input));
    }

}