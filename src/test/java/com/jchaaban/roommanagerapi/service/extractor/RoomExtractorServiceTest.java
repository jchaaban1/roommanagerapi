package com.jchaaban.roommanagerapi.service.extractor;

import com.jchaaban.roommanagerapi.entity.Room;
import com.jchaaban.roommanagerapi.rest.exception.person.InvalidFirstnameException;
import com.jchaaban.roommanagerapi.rest.exception.person.InvalidLastnameException;
import com.jchaaban.roommanagerapi.rest.exception.person.InvalidLdapException;
import com.jchaaban.roommanagerapi.rest.exception.room.InvalidRoomNumberException;
import com.jchaaban.roommanagerapi.service.validator.RoomNumberValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RoomExtractorServiceTest {

    @Mock
    private PersonExtractorService personExtractor;

    @Mock
    private RoomNumberValidator roomNumberValidator;

    private RoomExtractorService roomExtractorServiceToTest;

    private static Stream<Arguments> provideInvalidRoomNumbers() {
        return Stream.of(
                Arguments.of("111"),
                Arguments.of("11111"),
                Arguments.of("asdf")
        );
    }

    @BeforeEach
    void setUp() {
        roomExtractorServiceToTest = new RoomExtractorService(personExtractor, roomNumberValidator);
    }

    @Test
    @DisplayName("Valid room with multiple persons")
    void validRoomWithMultiplePersons() {
        String line = "1234,Dr. Jan Renken (jrenken),Marcel Schmidt (mschmidt)";

        Room room = roomExtractorServiceToTest.toRoom(line);

        assertThat(room).isNotNull();
        assertThat(room.getRoomNumber()).isEqualTo("1234");

        verify(roomNumberValidator).validate("1234");
        verify(personExtractor, times(2)).toPerson(anyString());
    }

    @ParameterizedTest
    @MethodSource("provideInvalidRoomNumbers")
    @DisplayName("Room with invalid room number throws InvalidRoomNumberException")
    void invalidRoomNumberThrowsInvalidRoomNumberException(String invalidRoomNumber) {
        String line = invalidRoomNumber + ",Dr. Jan Renken (jrenken)";
        doThrow(new InvalidRoomNumberException(invalidRoomNumber)).when(roomNumberValidator).validate(anyString());
        assertThatThrownBy(() -> roomExtractorServiceToTest.toRoom(line))
                .isInstanceOf(InvalidRoomNumberException.class);
    }

    @Test
    @DisplayName("Valid room with person having invalid first name throws InvalidFirstnameException")
    void validRoomWithPersonHavingInvalidFirstNamePersonThrowsInvalidFirstnameException() {
        String line = "1234,von Renken (vrenken)";

        doThrow(new InvalidFirstnameException(new String[]{"Jan"}))
                .when(personExtractor).toPerson(anyString());

        assertThatThrownBy(() -> roomExtractorServiceToTest.toRoom(line))
                .isInstanceOf(InvalidFirstnameException.class);
    }

    @Test
    @DisplayName("Valid room with person having invalid last name throws InvalidLastnameException")
    void validRoomWithPersonHavingInvalidLastNameThrowsInvalidLastnameException() {
        String line = "1234,Dr. Jan von (jvon)";

        doThrow(new InvalidLastnameException(new String[]{"von", "Something", "(jvon)"}))
                .when(personExtractor).toPerson(anyString());

        assertThatThrownBy(() -> roomExtractorServiceToTest.toRoom(line))
                .isInstanceOf(InvalidLastnameException.class);
    }

    @Test
    @DisplayName("Valid room with person having invalid ldap throws InvalidLdapException")
    void validRoomWithPersonHavingInvalidLdapThrowsInvalidLdapException() {
        String line = "1234,Dr. Jan Renken brenken";

        doThrow(new InvalidLdapException(new String[]{"Dr.", "Jan", "Renken", "jrenken"}))
                .when(personExtractor).toPerson(anyString());

        assertThatThrownBy(() -> roomExtractorServiceToTest.toRoom(line))
                .isInstanceOf(InvalidLdapException.class);
    }

}