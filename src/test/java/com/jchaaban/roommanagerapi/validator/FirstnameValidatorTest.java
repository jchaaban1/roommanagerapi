package com.jchaaban.roommanagerapi.validator;

import com.jchaaban.roommanagerapi.rest.exception.person.InvalidFirstnameException;
import com.jchaaban.roommanagerapi.service.validator.FirstnameValidator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

class FirstnameValidatorTest {

    private final FirstnameValidator firstnameValidatorToTest = new FirstnameValidator();


    public static Stream<Arguments> provideValidFirstnamePersonData() {
        return Stream.of(
            Arguments.of((Object) new String[] {"", "Tom", "", "Fischer", "(dfischer)"}),
            Arguments.of((Object) new String[] {"Dr.", "Tom", "", "Fischer", "(dfischer)"}),
            Arguments.of((Object) new String[] {"Dr.", "Tom", "von", "Fischer", "(dfischer)"}),
            Arguments.of((Object) new String[] {"Dr.", "Tom Peter", "von", "Fischer", "(dfischer)"})
        );
    }

    @ParameterizedTest
    @MethodSource("provideValidFirstnamePersonData")
    public void doesNotThrowWhenFirstnameIsValid(String[] personData) {
        assertDoesNotThrow(() -> firstnameValidatorToTest.validate(personData));
    }

    @Test
    @DisplayName("Throws InvalidPersonDataException when title exists instead of firstname")
    public void throwsExceptionWhenTitleExistsInsteadOfFirstname() {
        String[] personData = { "Dr.", "Fischer", "(dfischer)" };

        assertThatThrownBy(() -> firstnameValidatorToTest.validate(personData))
                .isInstanceOf(InvalidFirstnameException.class);
    }

    @Test
    @DisplayName("Throws InvalidPersonDataException when name addition exists instead of firstname")
    public void throwsExceptionWhenNameAdditionExistsInsteadOfFirstname() {
        String[] personData = { "von", "Fischer", "(dfischer)" };

        assertThatThrownBy(() -> firstnameValidatorToTest.validate(personData))
                .isInstanceOf(InvalidFirstnameException.class);
    }

    @Test
    @DisplayName("Throws InvalidPersonDataException when title and name addition exist instead of firstname")
    public void throwsWhenTitleAndNameAdditionExistInsteadOfFirstname() {
        String[] personData = { "Dr.", "von", "Fischer", "(dfischer)" };

        assertThatThrownBy(() -> firstnameValidatorToTest.validate(personData))
                .isInstanceOf(InvalidFirstnameException.class);
    }


}
