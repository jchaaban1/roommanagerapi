package com.jchaaban.roommanagerapi.validator;

import com.jchaaban.roommanagerapi.rest.exception.person.InvalidLastnameException;
import com.jchaaban.roommanagerapi.service.validator.LastnameValidator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

class LastnameValidatorTest {

    private final LastnameValidator lastnameValidatorToTest = new LastnameValidator();

    public static Stream<Arguments> provideValidLastnamePersonData() {
        return Stream.of(
                Arguments.of((Object) new String[] { "", "Tom", "", "Fischer", "(dfischer)" }),
                Arguments.of((Object) new String[] { "Dr.", "Tom", "", "Fischer", "(dfischer)" }),
                Arguments.of((Object) new String[] { "Dr.", "Tom", "von", "Fischer", "(dfischer)" }),
                Arguments.of((Object) new String[] { "Dr.", "Tom Peter", "von", "Fischer", "(dfischer)" })
        );
    }

    @ParameterizedTest
    @MethodSource("provideValidLastnamePersonData")
    public void doesNotThrowWhenLastnameIsValid(String[] personData) {
        assertDoesNotThrow(() -> lastnameValidatorToTest.validate(personData));
    }

    @Test
    @DisplayName("Throws an InvalidLastnameException when an namae addition exist instead of a lastname")
    public void throwsExceptionWhenNameAdditionExistsInsteadOfLastname() {
        String[] personData = { "Dennis", "von", "(dfischer)" };

        assertThatThrownBy(() -> lastnameValidatorToTest.validate(personData))
                .isInstanceOf(InvalidLastnameException.class);
    }
}