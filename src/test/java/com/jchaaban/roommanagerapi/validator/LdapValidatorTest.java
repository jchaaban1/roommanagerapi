package com.jchaaban.roommanagerapi.validator;

import com.jchaaban.roommanagerapi.rest.exception.person.InvalidLdapException;
import com.jchaaban.roommanagerapi.service.validator.LdapValidator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

class LdapValidatorTest {

    private final LdapValidator ldapValidatorToTest = new LdapValidator();


    @Test
    @DisplayName("Throws an InvalidLdapException when Ldap do not exist")
    public void throwsExceptionWhenLdapDoNotExist() {
        String[] personData = { "Dennis", "Fischer", "dfischer" };

        assertThatThrownBy(() -> ldapValidatorToTest.validate(personData))
                .isInstanceOf(InvalidLdapException.class);
    }

    @Test
    @DisplayName("The validate method of the LdapValidator don't throw an exception when ldap is valid")
    public void doesNotThrowWhenLdapIsValid() {
        String[] personData = {"Dr.", "Tom", "von", "Fischer", "(dfischer)"};
        assertDoesNotThrow(() -> ldapValidatorToTest.validate(personData));
    }
}
