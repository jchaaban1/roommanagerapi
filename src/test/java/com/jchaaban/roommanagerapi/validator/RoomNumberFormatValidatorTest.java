package com.jchaaban.roommanagerapi.validator;

import com.jchaaban.roommanagerapi.rest.exception.room.InvalidRoomNumberException;
import com.jchaaban.roommanagerapi.service.validator.RoomNumberValidator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

class RoomNumberFormatValidatorTest {

    private final RoomNumberValidator roomNumberFormatValidatorToTest = new RoomNumberValidator();

    public static Stream<Arguments> provideInvalidRoomNumbers() {
        return Stream.of(
                Arguments.of("11111"),
                Arguments.of("111"),
                Arguments.of("Dennis Fischer (dfischer)"),
                Arguments.of("Denn")
        );
    }

    @ParameterizedTest
    @MethodSource("provideInvalidRoomNumbers")
    public void invalidRoomNumberLength(String roomNumber) {
        assertThatThrownBy(() -> roomNumberFormatValidatorToTest.validate(roomNumber))
                .isInstanceOf(InvalidRoomNumberException.class)
                .hasMessageContaining(roomNumber);
    }

    @Test
    @DisplayName("The validate method of the RoomNumberFormatValidator don't throw an exception when roomNumber is valid")
    public void doesNotThrowWhenRoomNumberValid() {
        assertDoesNotThrow(() -> roomNumberFormatValidatorToTest.validate("1111"));
    }
}