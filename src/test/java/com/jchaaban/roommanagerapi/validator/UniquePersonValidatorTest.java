package com.jchaaban.roommanagerapi.validator;

import com.jchaaban.roommanagerapi.rest.exception.person.DuplicatedPersonException;
import com.jchaaban.roommanagerapi.service.validator.UniquePersonValidator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class UniquePersonValidatorTest {

    private final UniquePersonValidator uniquePersonValidatorToTest = new UniquePersonValidator();

    @Test
    @DisplayName("Throws a DuplicatedPersonException when a person exists multiple times in the file content (lines)")
    public void throwsDuplicatedPersonException() {
        List<String> lines = Arrays.asList(
                "1111,Dennis Fischer (dfischer),Dr. Frank von Supper (fsupper),Susanne Moog (smoog)",
                "1110,Christina Hülsemann (chuelsemann),Iftikar Ahmad Khan (ikhan),Mabelle Tengue (mtengue),Ralf Schmidt (rschmidt)",
                "1109,Stefanie Borcherding (sborcherding),Tobias Hahn (thahn),Dominik Elm (delm)",
                "1108,Kai Wesling (kwesling),Thomas Kruse (tkruse)",
                "1107,Carsten Schütte (cschuette),Carsten Elfers (celfers),Nicole Dymini (ndymini),Susanne Moog (smoog)"
        );

        assertThatThrownBy(() -> uniquePersonValidatorToTest.validate(lines))
                .isInstanceOf(DuplicatedPersonException.class)
                .hasMessageContaining("Susanne Moog (smoog)");
    }
    @Test
    @DisplayName("Do not throw an DuplicatedPersonException when persons are unique")
    public void doesNotThrowExceptionWhenPersonsAreUnique() {
        List<String> lines = Arrays.asList(
                "1111,Dennis Fischer (dfischer),Dr. Frank von Supper (fsupper),Susanne Moog (smoog)",
                "1110,Christina Hülsemann (chuelsemann),Iftikar Ahmad Khan (ikhan),Mabelle Tengue (mtengue),Ralf Schmidt (rschmidt)"
        );

        assertDoesNotThrow(() -> uniquePersonValidatorToTest.validate(lines));
    }

}
