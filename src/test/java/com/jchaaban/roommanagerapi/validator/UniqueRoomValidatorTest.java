package com.jchaaban.roommanagerapi.validator;

import com.jchaaban.roommanagerapi.rest.exception.room.DuplicatedRoomException;
import com.jchaaban.roommanagerapi.service.validator.UniqueRoomValidator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

class UniqueRoomValidatorTest {

    private final UniqueRoomValidator uniqueRoomValidatorToTest = new UniqueRoomValidator();

    @Test
    @DisplayName("Throws DuplicatedRoomException when the same room number exist multiple times")
    public void throwsDuplicatedRoomExceptionWithDuplicatedRoomNumbers() {
        List<String> lines = List.of(
                "1000, Bernd Reiners (breiners)",
                "2000, Max Mustermann mmustermann",
                "1000, Jaafar Chaaban (jchaaban)"
        );

        assertThatThrownBy(() -> uniqueRoomValidatorToTest.validate(lines))
                .isInstanceOf(DuplicatedRoomException.class)
                .hasMessageContaining("1000");
    }
    @Test
    @DisplayName("Do not throw an DuplicatedRoomsException when rooms are unique")
    public void doesNotThrowExceptionWhenRoomsAreUnique() {
        List<String> lines = List.of(
                "1000, Bernd Reiners (breiners)",
                "1001, Jaafar Chaaban (jchaaban)",
                "2000, Max Mustermann mmustermann"
        );

        assertDoesNotThrow(() -> uniqueRoomValidatorToTest.validate(lines));
    }
}
